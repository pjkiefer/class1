/*Exercise 1*/
//function to get random number
function getRandomInt(min,max) {
    min = Math.ceil(min);
  	max = Math.floor(max);
  	return Math.floor(Math.random() * (max - min + 1)) + min;
}

//draw cards
var card1 = getRandomInt(1,13); // 4
var card2 = getRandomInt(1,13); // 8
var card3 = getRandomInt(1,13); // 2

//get max number
var max = Math.max(card1, card2, card3); // 8

//pizza...
function getPizzaArea(radius){
	return Math.PI * radius * radius;
}

var pizza13 = getPizzaArea(13); // 530.929158456675
var pizza17 = getPizzaArea(17); // 907.9202768874503

var cost13 = 16.99 / pizza13; // 0.03200050275895031
var cost17 = 19.99 / pizza17; // 0.02201735164295493


/*Exercise 2*/
var firstName = 'Tom';
var lastName = 'Smith';
var streetAddress = '13 Pine St';
var city = 'Redburg';
var state = 'WA';
var zip = '12345';

var printAddress = firstName + ' ' + lastName + '\n' + streetAddress + '\n' + city + ', ' + state + ' ' + zip;
/*"Tom Smith
13 Pine St
Redburg, WA 12345"*/

var addressArray = printAddress.split('\n'); // ["Tom Smith", "13 Pine St", "Redburg, WA 12345"]
var name = addressArray[0].split(' ');
var first = name[0];
var last = name[1];

var address = addressArray[1];


//A couple of function to determine if 5 or 9 diget zip (9 + hyphen)
//check if character is a number or -
function checkForNumber(x){
    if(x >= '0' && x <= '9' || x == '-')
        return true;
    else
        return false;     
}

//check if zip is 5 digets or 5 digets/-/4 digets
function checkForZip(v){
    if(checkForNumber(x.substr(x.length - 6) == false))
       return 5;
    else
        return 10;
       
}

//get the comma after the city name
var comma = addressArray[2].indexOf(','); // 7
	
var city = addressArray[2].substr(0, comma); // Redburg

//find the lenth of the zip
var zipLength = checkForZip(addressArray[2]); // 5

//take out the city and work with the rest
var tmp = addressArray[2].substr(comma+2, addressArray[2].length - 1); // "WA 12345"
//get the state based on the zip length
var state = tmp.substr(0, tmp.length - (zipLength + 1)); // WA
//finally pull the zip 
var zip = tmp.substr(tmp.length - (zipLength));


/*Dates*/
var date1 = new Date(2019,0,1);
var date2 = new Date(2019,3,1);
var middleDate = new Date((date1.getTime() + date2.getTime()) / 2); // Thu Feb 14 2019 23:30:00 GMT-0800 (Pacific Standard Time)